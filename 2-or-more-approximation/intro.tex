\documentclass{article}

\usepackage{mathpartir}
\usepackage{termination_lib}

\begin{document}

The correspondence between natural-deduction proofs of propositional
intuitionistic logic, usually written as (logic) derivations for
judgments of the form $\ca \der \ta$, and well-typed terms in the
simply-typed lambda-calculus, with (typing) derivations for the
judgment $\ca \der \ea : \ta$, is not one-to-one. In typing judgments
$\ca \der \ea : \ta$, the context $\ca$ is a mapping from free
variables to their type. In logic derivations, the context $\ca$ is
a set of hypotheses; there is no notion of variable, and at most one
hypothesis of each type in the set. This means, for example, that the
following logic derivation
%
\begin{mathpar}
  \infer
  {\infer
  {\infer{ }{\ta \der \ta}}
  {\ta \der \arr{\ta}{\ta}}}
  {\emptyset \der \arr{\ta}{\arr{\ta}{\ta}}}
\end{mathpar}
%
corresponds to two \emph{distinct} programs, namely
$\lam{\eva}{\lam{\evb}{\eva}}$ and $\lam{\eva}{\lam{\evb}{\evb}}$. We
say that those programs have the same \emph{shape}, in the sense that
the erasure of their typing derivation gives the same logic
derivation -- and they are the only programs of this shape.

Despite, or because, not being one-to-one, this correspondence is very
helpful to answer questions about type systems. For example, the
question of whether, in a given typing environment $\ca$, the type
$\ta$ is inhabited, can be answered by looking instead for a valid
logic derivation of $\erase{}{\ca} \der \ta$, where $\erase{}{\ca}$
denotes the erasure of the mapping $\ca$ into a set of hypotheses. If
we can independently prove that only a finite number of different
types need to be considered to find a valid proof (this is the case
for propositional logic because of the \emph{subformula property}),
then there are finitely many set-of-hypothesis $\cb$, and the
search space of sequents $\cb \der \tb$ to consider during proof
search is finite. This property is key to the termination of most
search algorithms for the simply-typed lambda-calculus. Note that it
would not work if we searched typing derivations $\ca \der \ea : \ta$
directly: even if there are finitely many types of interest, the set
of mappings from variables to such types is infinite.

In our current brand of work, we are interested in a different
problem. Instead of knowing whether there \emph{exists} a term $\ea$
such that $\ca \der \ea : \ta$, we want to know whether this term is
\emph{unique} -- modulo a given notion of program
equivalence. Intuitively, this can be formulated as a search problem
where search does not stop to the first candidate, but tries to find
whether a second one (that is nonequivalent as a program) exists. In
this setting, the technique of searching for logic derivations
$\erase{}{\ca} \der \ta$ instead is not enough, because a unique logic
derivation may correspond to several distinct programs of this shape:
summarizing typing environments as set-of-hypotheses loses information
about (non)-unicity.

To better preserve this information, one could keep track of the
number of times an hypothesis has been added to the context,
representing contexts as \emph{multisets} of hypotheses; given a logic
derivation annotated with such counts in the context, we can precisely
compute the number of programs of this shape. However, even for
a finite number of types/formulas, the space of such multisets is
infinite; this breaks termination arguments. A natural idea is then to
\emph{approximate} multisets by labeling hypotheses with $0$
(not available in the context), $1$ (added exactly once), or $\tp$
(available two times \emph{or more}); this two-or-more approximation
has three possible states, and there are thus finitely many contexts
annotated in this way.

The question we answer in this note is the following: is the
two-or-more approximation correct? By correct, we mean that if the
\emph{precise} number of times a given hypothesis is available varies,
but remains in the same approximation class, then the total number of
programs of this shape may vary, but will itself remain in the same
annotation class. A possible counter-example would be a logic
derivation $\cb \der \tb$ such that, if a given hypothesis
$\ta \in \cb$ is present exactly twice in the context (or has two free
variables of this type), there is one possible program of this shape,
but having three copies of this hypothesis would lead to several distinct
programs.

Is this approximation correct? We found it surprisingly difficult to
have an intuition on this question (guessing what the answer should
be), and discussions with colleagues indicate that there is no obvious
guess -- people have contradictory intuitions on this.

\end{document}