STOG=stog --package stog.disqus,stog.markdown --nocache
DEST_DIR=../gagallium-output
BASE_URL_OPTION=--site-url http://gallium.inria.fr/blog
PUBLICATION_LEVEL_OPTION=
STOG_OPTIONS=-d $(DEST_DIR) $(BASE_URL_OPTION) $(PUBLICATION_LEVEL_OPTION) --tmpl tmpl $(STOG_VERBOSE)
LESSC=lessc

STOG_TEST_DIR ?= /home/yquem/cristal/scherer/public_html/tmp/gagallium-test
STOG_TEST_URL ?= http://gallium.inria.fr/~scherer/tmp/gagallium-test

.PHONY: clean build site test

clean:
	rm -fr $(DEST_DIR)/*
	rm -f ocaml.log

build:
	rm -fr $(DEST_DIR)/*
	$(MAKE) site

site:
	time $(STOG) $(STOG_OPTIONS) .
	(cd less && $(LESSC) style.less > style.css)
	mv less/style.css $(DEST_DIR)/
	cp *.png tmpl/*png $(DEST_DIR)/ || true
	cp *.svg $(DEST_DIR)/ || true

# use the name of a directory, and only its blog post
# will be built; for example:
#  make only-resolving-field-names
only-%:
	@time $(STOG) $(STOG_OPTIONS) --only $* .
	# looks like --only and --no-cache do cache
	rm -fR .stog/cache/$*
	(cd less && $(LESSC) style.less > style.css)
	mv less/style.css $(DEST_DIR)/
	cp *.png tmpl/*png $(DEST_DIR)/ || true
	cp *.svg $(DEST_DIR)/ || true


test:
	$(MAKE) BASE_URL_OPTION="--site-url $(STOG_TEST_URL)" PUBLICATION_LEVEL_OPTION="--publication-level draft" DEST_DIR="../gagallium-test" build
	cp ../test-htaccess ../gagallium-test/.htaccess
	rm -fR $(STOG_TEST_DIR)
	cp -r ../gagallium-test $(STOG_TEST_DIR)

local:
	$(MAKE) BASE_URL_OPTION="--site-url file:///tmp/gagallium" PUBLICATION_LEVEL_OPTION="--publication-level draft" DEST_DIR="/tmp/gagallium" build
