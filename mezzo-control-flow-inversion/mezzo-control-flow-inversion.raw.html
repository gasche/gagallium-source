<h2 id="an-ownership-problem">An ownership problem</h2>
<p>Imagine you want to find one element among a list that satisfies a given predicate. In OCaml, this can be easily done:</p>
<div class="highlight"><pre><span class="k">let</span> <span class="k">rec</span> <span class="n">find0</span> <span class="o">(</span><span class="n">pred</span><span class="o">:</span> <span class="k">&#39;</span><span class="n">a</span> <span class="o">-&gt;</span> <span class="kt">bool</span><span class="o">)</span> <span class="o">(</span><span class="n">l</span><span class="o">:</span> <span class="k">&#39;</span><span class="n">a</span> <span class="kt">list</span><span class="o">):</span> <span class="k">&#39;</span><span class="n">a</span> <span class="n">option</span> <span class="o">=</span>
  <span class="k">match</span> <span class="n">l</span> <span class="k">with</span>
  <span class="o">|</span> <span class="bp">[]</span> <span class="o">-&gt;</span> <span class="nc">None</span>
  <span class="o">|</span> <span class="n">hd</span><span class="o">::</span><span class="n">tl</span> <span class="o">-&gt;</span>
      <span class="k">if</span> <span class="n">pred</span> <span class="n">hd</span> <span class="k">then</span>
        <span class="nc">Some</span> <span class="n">hd</span>
      <span class="k">else</span>
        <span class="n">find0</span> <span class="n">pred</span> <span class="n">tl</span>
</pre></div>



<p>However, there's an ownership problem here. Assuming an element is found, then doing the following breaks the ownership discipline.</p>
<div class="highlight"><pre><span class="k">let</span> <span class="n">l</span> <span class="o">=</span> <span class="o">...</span> <span class="k">in</span>
<span class="k">match</span> <span class="n">find0</span> <span class="o">...</span> <span class="n">l</span> <span class="k">with</span>
<span class="o">|</span> <span class="nc">Some</span> <span class="n">elt</span> <span class="o">-&gt;</span> <span class="c">(* Ownership problem here! *)</span>
<span class="o">|</span> <span class="nc">None</span> <span class="o">-&gt;</span> <span class="o">...</span>
</pre></div>



<p>Indeed, the element <code>elt</code> is reachable through two different, seemingly distinct paths: the variable <code>elt</code> itself, and the list <code>l</code> where the elements remains. In a language that tracks ownership, such as, but not limited to, Mezzo, this is a problem.</p>
<p>Indeed, if you want your <code>find</code> function to work for <em>any list of <code>a</code></em>, then your function cannot make any assumptions on <code>a</code>, and has to assume that elements of type <code>a</code> have a unique owner. The <code>find0</code> function above will thus be rejected by Mezzo.</p>
<p>An alternative solution would be to add an extra constraint on a caller of the function, and require that the type <code>a</code> be duplicable. There is a mechanism for that in Mezzo. Here, however, we wish to attain a more general solution.</p>
<h2 id="the-classic-solution">The classic solution</h2>
<p>The classic solution is to reverse the control-flow: instead of asking <code>find</code> to provide us with the element it found, we are going to provide <code>find</code> with whatever we want to do with the element. That is, we're going to pass a first-class function to <code>find</code>.</p>
<p>The signature of <code>find</code> thus becomes, in Mezzo:</p>
<div class="highlight"><pre><span class="k">val</span> <span class="n">find1</span><span class="o">:</span> <span class="o">[</span><span class="n">a</span><span class="o">]</span> <span class="o">(</span><span class="n">l</span><span class="o">:</span> <span class="n">list</span> <span class="n">a</span><span class="o">,</span> <span class="n">pred</span><span class="o">:</span> <span class="n">a</span> <span class="o">-&gt;</span> <span class="n">bool</span><span class="o">,</span> <span class="n">f</span><span class="o">:</span> <span class="n">a</span> <span class="o">-&gt;</span> <span class="o">())</span> <span class="o">-&gt;</span> <span class="o">()</span>
</pre></div>



<p>(We could have <code>f</code> return an element of type <code>b</code> and then have <code>find1</code> return <code>b</code> as well. We keep it simple for now.)</p>
<p>Remember that in Mezzo, unless marked with <code>consumes</code>, arguments are preserved (i.e. their ownership is taken <em>and</em> returned). The function above thus preserves a list <code>l</code>; it takes a predicate and a function <code>f</code>. The function <code>f</code> is allowed to operate on an element but must leave it untouched.</p>
<p>The function <code>f</code> may want to perform side-effects: we can provide a better signature, as follows.</p>
<div class="highlight"><pre><span class="k">val</span> <span class="n">find2</span><span class="o">:</span> <span class="o">[</span><span class="n">a</span><span class="o">,</span> <span class="n">s</span><span class="o">:</span> <span class="k">perm</span><span class="o">]</span> <span class="o">(</span><span class="n">l</span><span class="o">:</span> <span class="n">list</span> <span class="n">a</span><span class="o">,</span> <span class="n">pred</span><span class="o">:</span> <span class="n">a</span> <span class="o">-&gt;</span> <span class="n">bool</span><span class="o">,</span> <span class="n">f</span><span class="o">:</span> <span class="o">(</span><span class="n">a</span> <span class="o">|</span> <span class="n">s</span><span class="o">)</span> <span class="o">-&gt;</span> <span class="o">())</span> <span class="o">-&gt;</span> <span class="o">()</span>
</pre></div>



<p>One may use <code>find2</code> as follows.</p>
<div class="highlight"><pre><span class="k">let</span> <span class="n">l</span> <span class="o">=</span> <span class="o">...</span> <span class="k">in</span>
<span class="c">(* l @ list (ref int) *)</span>
<span class="k">let</span> <span class="n">found</span> <span class="o">=</span> <span class="n">ref</span> <span class="nc">None</span> <span class="k">in</span>
<span class="n">find2</span> <span class="o">(</span><span class="n">l</span><span class="o">,</span>
  <span class="o">(</span><span class="k">fun</span> <span class="o">(</span><span class="n">x</span><span class="o">:</span> <span class="n">ref</span> <span class="n">int</span><span class="o">):</span> <span class="n">bool</span> <span class="o">=</span> <span class="err">!</span><span class="n">x</span> <span class="o">&gt;</span> <span class="mi">2</span><span class="o">),</span>
  <span class="k">fun</span> <span class="o">(</span><span class="n">x</span><span class="o">:</span> <span class="n">ref</span> <span class="n">int</span> <span class="o">|</span> <span class="n">found</span> <span class="o">@</span> <span class="n">ref</span> <span class="o">(</span><span class="n">int</span> <span class="n">option</span><span class="o">)):</span> <span class="o">()</span> <span class="o">=</span>
    <span class="n">found</span> <span class="o">:=</span> <span class="nc">Some</span> <span class="o">(</span><span class="err">!</span><span class="n">x</span><span class="o">)</span>
<span class="o">);</span>
<span class="k">match</span> <span class="err">!</span><span class="n">found</span> <span class="k">with</span>
<span class="o">|</span> <span class="nc">Some</span> <span class="n">x</span> <span class="o">-&gt;</span>
    <span class="c">(* Found an integer greater than 2, it&#39;s x *)</span>
    <span class="o">...</span>
<span class="o">|</span> <span class="nc">None</span> <span class="o">-&gt;</span>
    <span class="o">...</span>
<span class="k">end</span>
</pre></div>



<p>This is awkward for a variety of reasons. First, the control-flow feels very unnatural. We're actually losing control. Second, writing first-class functions requires a lot of annotations. Finally, <code>find2</code> is actually no more useful than a regular <code>iter</code> function.</p>
<h2 id="the-better-solution">The better solution</h2>
<p>Turns out (ta-da!) we can actually do better. We can write this function in &quot;direct style&quot; (i.e. have the soon-to-be <code>find3</code> return the element to its caller) while still maintaining ownership guarantees. Without any runtime test!</p>
<p>The key insight is to have a <code>restore</code> function that <em>consumes</em> the ownership of the element in order to <em>regain</em> the ownership of the original list. This, of course, will rely a little bit on dependent types.</p>
<div class="highlight"><pre><span class="k">alias</span> <span class="n">restore_t</span> <span class="o">(</span><span class="n">x</span><span class="o">:</span> <span class="k">term</span><span class="o">)</span> <span class="n">a</span> <span class="o">(</span><span class="n">l</span><span class="o">:</span> <span class="k">term</span><span class="o">)</span> <span class="o">=</span> 
  <span class="o">{</span> <span class="n">p</span><span class="o">:</span> <span class="k">perm</span> <span class="o">}</span> <span class="o">(</span>
    <span class="o">(|</span> <span class="k">consumes</span> <span class="o">(</span><span class="n">p</span> <span class="o">*</span> <span class="n">x</span> <span class="o">@</span> <span class="n">a</span><span class="o">))</span> <span class="o">-&gt;</span> <span class="o">(|</span> <span class="n">l</span> <span class="o">@</span> <span class="n">list</span> <span class="n">a</span><span class="o">)</span> <span class="o">|</span> <span class="n">p</span>
  <span class="o">)</span>
</pre></div>



<p>An element of type <code>restore_t x a l</code> is a package of a function along with an existentially-quantified permission <code>p</code>. The function, in order to be called, requires <code>x @ a * p</code>, which it <em>consumes</em>. In exchange, one gains <code>l @ list a</code>.</p>
<p>Phrased differently, this is a one-shot function (<code>p</code> is existentially-quantified, so the function can only be called once), which <em>consumes</em> <code>x @ a</code> to obtain <code>l @ list a</code>.</p>
<p>Since the permission <code>p</code> is existentially-quantified, we must treat it as affine. Because calling <code>f</code> requires <code>p</code>, after <code>f</code> has been called once, <code>p</code> is gone, meaning that <code>f</code> can no longer be called: this is effectively a one-shot function. The iterator blog post refers to this as a magic wand from <code>x @ a</code> to <code>l @ list a</code>.</p>
<p>As you may have guessed by now, the signature of <code>find3</code> is as follows.</p>
<div class="highlight"><pre><span class="k">val</span> <span class="n">find3</span><span class="o">:</span> <span class="o">[</span><span class="n">a</span><span class="o">]</span> <span class="o">(</span><span class="k">consumes</span> <span class="n">l</span><span class="o">:</span> <span class="n">list</span> <span class="n">a</span><span class="o">,</span> <span class="n">pred</span><span class="o">:</span> <span class="n">a</span> <span class="o">-&gt;</span> <span class="n">bool</span><span class="o">)</span> <span class="o">-&gt;</span>
    <span class="n">either</span> <span class="o">(|</span> <span class="n">l</span> <span class="o">@</span> <span class="n">list</span> <span class="n">a</span><span class="o">)</span> <span class="o">(</span><span class="n">x</span><span class="o">:</span> <span class="n">a</span><span class="o">,</span> <span class="n">restore_t</span> <span class="n">x</span> <span class="n">a</span> <span class="n">l</span><span class="o">)</span>
</pre></div>



<p>The function takes, just like <code>find0</code>, a list <code>l</code> and a predicate function <code>pred</code>. The return type is a sum. In the <code>Left</code> case, no element has been found, so the unit value is returned, along with full ownership of the list. In the <code>Right</code> case, the caller does <em>not</em> regain ownership of the list <code>l</code>. What it gains instead is an element <code>x</code> (the element that was found) along with the <code>restore</code> function we just saw.</p>
<p>Here's some sample client code for <code>find3</code> which, as you can see, is written in the same, natural style as the OCaml code for <code>find0</code>, except for the call to <code>restore</code> (but well, our language tracks ownership, so the user will have to talk about ownership too...).</p>
<div class="highlight"><pre><span class="k">val</span> <span class="n">_</span> <span class="o">=</span>
  <span class="c">(* Create a sample list. *)</span>
  <span class="k">let</span> <span class="n">l</span> <span class="o">=</span> <span class="n">cons</span> <span class="o">(</span><span class="n">newref</span> <span class="mi">1</span><span class="o">,</span> <span class="n">cons</span> <span class="o">(</span><span class="n">newref</span> <span class="mi">2</span><span class="o">,</span> <span class="n">nil</span><span class="o">))</span> <span class="k">in</span>
  <span class="c">(* Try to find an element greater than 1 *)</span>
  <span class="k">match</span> <span class="n">find</span> <span class="o">(</span><span class="n">l</span><span class="o">,</span> <span class="k">fun</span> <span class="o">(</span><span class="n">x</span><span class="o">:</span> <span class="n">ref</span> <span class="n">int</span><span class="o">):</span> <span class="n">_</span> <span class="o">=</span> <span class="err">!</span><span class="n">x</span> <span class="o">&gt;</span> <span class="mi">1</span><span class="o">)</span> <span class="k">with</span>
  <span class="o">|</span> <span class="nc">Left</span> <span class="o">-&gt;</span>
      <span class="c">(* No such element has been found *)</span>
      <span class="o">()</span>
  <span class="o">|</span> <span class="nc">Right</span> <span class="o">{</span> <span class="n">contents</span> <span class="o">=</span> <span class="o">(</span><span class="n">elt</span><span class="o">,</span> <span class="n">restore</span><span class="o">)</span> <span class="o">}</span> <span class="o">-&gt;</span>
      <span class="c">(* The element [elt] has been found. *)</span>
      <span class="n">print</span> <span class="n">elt</span><span class="o">;</span>
      <span class="c">(* Calling the (ghost) [restore] function allows one to give up ownership</span>
<span class="c">       * of [elt] and recover ownership of [l] instead. *)</span>
      <span class="n">restore</span> <span class="o">()</span>
  <span class="k">end</span><span class="o">;</span>
  <span class="c">(* In any case, we can use [l] afterwards. *)</span>
  <span class="k">assert</span> <span class="n">l</span> <span class="o">@</span> <span class="n">list</span> <span class="o">(</span><span class="n">ref</span> <span class="n">int</span><span class="o">)</span>
</pre></div>



<p>In can hear the impatient readers eager to see the implementation of <code>find3</code>. Behold! Here it is.</p>
<div class="highlight"><pre><span class="k">val</span> <span class="k">rec</span> <span class="n">find3</span> <span class="o">[</span><span class="n">a</span><span class="o">]</span> <span class="o">(</span><span class="k">consumes</span> <span class="n">l</span><span class="o">:</span> <span class="n">list</span> <span class="n">a</span><span class="o">,</span> <span class="n">pred</span><span class="o">:</span> <span class="n">a</span> <span class="o">-&gt;</span> <span class="n">bool</span><span class="o">):</span>
    <span class="n">either</span> <span class="o">(|</span> <span class="n">l</span> <span class="o">@</span> <span class="n">list</span> <span class="n">a</span><span class="o">)</span> <span class="o">(</span><span class="n">x</span><span class="o">:</span> <span class="n">a</span><span class="o">,</span> <span class="n">restore_t</span> <span class="n">x</span> <span class="n">a</span> <span class="n">l</span><span class="o">)</span>
  <span class="o">=</span>
  <span class="k">match</span> <span class="n">l</span> <span class="k">with</span>
  <span class="o">|</span> <span class="nc">Nil</span> <span class="o">-&gt;</span>
      <span class="c">(* We found no suitable element. *)</span>
      <span class="n">left</span> <span class="o">()</span>
  <span class="o">|</span> <span class="nc">Cons</span> <span class="o">{</span> <span class="n">head</span><span class="o">;</span> <span class="n">tail</span> <span class="o">}</span> <span class="o">-&gt;</span>
      <span class="k">if</span> <span class="n">pred</span> <span class="n">head</span> <span class="k">then</span>
        <span class="c">(* We found the element we&#39;re looking for! *)</span>
        <span class="n">right</span> <span class="o">(</span>
          <span class="n">head</span><span class="o">,</span>
          <span class="k">fun</span> <span class="o">(|</span> <span class="k">consumes</span> <span class="o">(</span><span class="n">head</span> <span class="o">@</span> <span class="n">a</span> <span class="o">*</span> <span class="n">tail</span> <span class="o">@</span> <span class="n">list</span> <span class="n">a</span><span class="o">)):</span> <span class="o">(|</span> <span class="n">l</span> <span class="o">@</span> <span class="n">list</span> <span class="n">a</span><span class="o">)</span> <span class="o">=</span>
            <span class="c">(* Here&#39;s permission p:   ^^^^^^^^^^^^^ *)</span>
            <span class="o">())</span>
      <span class="k">else</span>
        <span class="c">(* The element we&#39;re looking for may be further away. *)</span>
        <span class="k">match</span> <span class="n">find3</span> <span class="o">(</span><span class="n">tail</span><span class="o">,</span> <span class="n">pred</span><span class="o">)</span> <span class="k">with</span>
        <span class="o">|</span> <span class="nc">Left</span> <span class="o">-&gt;</span>
            <span class="n">left</span> <span class="o">()</span>
        <span class="o">|</span> <span class="nc">Right</span> <span class="o">{</span> <span class="n">contents</span> <span class="o">=</span> <span class="o">(</span><span class="n">elt</span><span class="o">,</span> <span class="n">restore</span><span class="o">)</span> <span class="o">}</span> <span class="o">-&gt;</span>
            <span class="k">let</span> <span class="k">flex</span> <span class="n">s</span><span class="o">:</span> <span class="k">perm</span> <span class="k">in</span>
            <span class="n">right</span> <span class="o">(</span>
              <span class="n">elt</span><span class="o">,</span>
              <span class="k">fun</span> <span class="o">(|</span> <span class="k">consumes</span> <span class="o">(</span><span class="n">elt</span> <span class="o">@</span> <span class="n">a</span> <span class="o">*</span> <span class="n">head</span> <span class="o">@</span> <span class="n">a</span> <span class="o">*</span> <span class="n">s</span><span class="o">)):</span> <span class="o">(|</span> <span class="n">l</span> <span class="o">@</span> <span class="n">list</span> <span class="n">a</span><span class="o">)</span> <span class="o">=</span>
              <span class="c">(* Here&#39;s permission p:    ^^^^^^^^^^^^ *)</span>
                <span class="n">restore</span> <span class="o">())</span>
        <span class="k">end</span>
  <span class="k">end</span>
</pre></div>



<p>It's a 20-liner, so don't be afraid, and let's break it down.</p>
<h3 id="boring-case">Boring case</h3>
<p>In the case that we've reached the end of the list, we return the <code>left</code> injection with the unit type. The ownership of the list is properly returned to the caller: it's the empty list.</p>
<h3 id="interesting-case">Interesting case</h3>
<p>Let us now turn to the first interesting case: the element we've hit is precisely the one we were looking for.</p>
<p>We return <code>head</code>: this corresponds to the <code>x</code> we wrote in the function signature.</p>
<p>Let us now examine the definition of <code>restore</code>. It requires the permission for <code>head</code>: this is line with the definition of <code>restore_t</code>. The definition of <code>restore_t</code> also mentions a certain permission <code>p</code>: in our case, this is <code>tail @ list a</code>. The <code>restore</code> function also captures the duplicable permission <code>l @ Cons { head = head; tail = tail }</code>: duplicable permissions <em>can</em> be captured in Mezzo. Combining these three permissions, without performing any run-time operation, <code>restore</code> returns full ownership for the list <code>l</code>.</p>
<p>In Mezzo lingo: <code>head @ a * tail @ list a * l @ Cons { head = head; tail = tail } ≤ l @ list a</code>.</p>
<p>There an existential-packing involved: the type-checker performs this automatically and packs <code>tail @ list a</code> as <code>p</code> (this is real, unedited Mezzo code that I'm showing).</p>
<p>One can glance briefly further down, to see that in the last case, the actual permission <code>p</code> is a different one. Without the existential quantification, the types would fail to match. The actual value of <code>p</code> (&quot;existential witness&quot;) depends on the case.</p>
<h3 id="actually-not-boring-case">Actually not-boring case</h3>
<p>In the case that the element failed to be found further down the list, the recursive call to <code>find3</code> returns <code>Left</code>. Even though the code is terse, let us break down what happens.</p>
<p>In the <code>Left</code> branch, we learn that we regain full ownership of whatever we called <code>find3</code> with: in our case, we gain <code>tail @ list a</code>. The type-checker knows that we still possess <code>head @ a * l @ Cons { head = head; tail = tail }</code>: it thus recombines the three permissions to show that <code>left ()</code> actually produces <code>Left { contents = () } | l @ list a</code>, per the return type of <code>find3</code>.</p>
<p>There's a lot of under-the-hood machinery that is completely transparent: I'm going to be bold and declare that I find this pretty cool.</p>
<h3 id="very-interesting-case">Very interesting case</h3>
<p>The last case is, naturally, the most interesting one. The element we're looking for has actually been found further down the list. The recursive call provided us with: the element <code>elt</code>, along with <code>elt @ a</code>; a function <code>restore</code> that <em>consumes</em> <code>elt @ a</code> and returns us <code>tail @ a</code>; a certain permission that <code>restore</code> needs in order to be called. It's the existentially-quantified one. Let's name it <code>s</code>.</p>
<p>We return the element we've found, <code>elt</code>, along with a function that <em>consumes</em> <code>elt @ a</code> and returns full ownership of the list. How do we write that function?</p>
<p>That function will <em>consume</em> <code>elt @ a</code>, which is sort of expected. It also needs other permissions: it needs the &quot;certain permission&quot; <code>s</code>, in order to call the inner <code>restore</code> function. We also need <code>head @ a</code> if we are to piece back everything together to recreate ownership of <code>l</code>.</p>
<p>Entering the <em>new</em> <code>restore</code> function, we have <code>elt @ a * s * head @ a</code>. Calling <code>restore</code> <em>consumes</em> <code>elt @ a * s</code>, and gives <code>tail @ a</code>. The rest of the story is familiar: all is well and ownership of <code>l</code> is returned.</p>
<h2 id="wrapping-it-up">Wrapping it up</h2>
<p>In order for the story to have a happy ending, we must somehow get rid of the run-time operations for <code>restore</code> which is, in essence, a no-op. Turns out the story's not that good: we don't have ghost functions in Mezzo. That's a pretty big deal actually: it's a major feature to add into the language. One needs to think carefully about termination criteria. In our example, since our data types are inductive, and never <em>co-inductive</em>, one can use the length of the list to show that <code>restore</code> only ever calls itself with a smaller parameter. But imagine we have integers: should we use an external solver to prove that <code>f(x) &lt; g(x)</code>? Should we write our own? What about other termination criteria?</p>
<p>So it's a little bit frustrating that the run-time operations are still there. Hopefully that's something that we'll fix at some point. It seems there's still some valuable research to be done about Mezzo...</p>
