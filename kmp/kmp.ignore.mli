(* An abstract type of compiled patterns. *)
type pattern

(* An abstract type of states. *)
type state

(* The initial state is used to begin a search. *)
val initial : state

(* A search result is either an offset in the text where the pattern
   was found, or a state, which means that the end of the text was
   reached in that state. *)
type result =
| Found of int
| Interrupted of state

(* [compile p] turns the pattern [p], represented as a string, into
   a compiled pattern. Its time complexity is linear in the size of
   the pattern. *)
val compile: string -> pattern

(* [find p s text k n] searches for the compiled pattern [p] in the
   string [text], between the offsets [k] included and [n] excluded. Its
   time complexity is linear with respect to [n - k].  A search normally
   begins in the [initial] state, but a state [s] returned by a previous
   search can also be used, if desired; this feature allows interrupting
   and resuming a search. *)
val find: pattern -> state -> string -> int -> int -> result

